﻿using System;

namespace BTree
{
    public class Entry<T, V> : IEquatable<Entry<T, V>>
    {
        public T Key { get; }
        public V Value { get; set; }

        public Entry(T key, V value)
        {
            Key = key;
            Value = value;
        }
        
        public bool Equals(Entry<T, V> other) => Key.Equals(other.Key) && Value.Equals(other.Value);
        public override string ToString() => Key.ToString();
    }
}
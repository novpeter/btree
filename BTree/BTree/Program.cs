﻿using System;

namespace BTree
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /* Example
                var btree = new BTree<int, int>(3);
                btree.Insert(1, 1);
                btree.Insert(2, 2);
                btree.Insert(3, 3);
                btree.Insert(4, 4);
                btree.Insert(5, 5);
                btree.Insert(6, 6);
                btree.Insert(7, 7);
                btree.Remove(6);
                Console.WriteLine(btree.Search(7));
                btree.Insert(7, 8);
                Console.WriteLine(btree.Search(7));
            */
            
            /* For tests
                var btree = new BTree<int, int>(3);
                btree.Insert(10, 10);
                btree.Insert(20, 20);
                btree.Insert(30, 30);
                btree.Insert(40, 40);
                btree.Insert(50, 50);
                btree.Insert(60, 60);
                btree.Insert(70, 70);
                btree.Insert(80, 80);
                btree.Insert(90, 90);
                btree.Insert(100, 100);
                btree.Insert(110, 110);
                btree.Insert(120, 120);
            */
        }
    }
}

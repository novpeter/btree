﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Linq;

namespace BTree
{
    [TestFixture]
    public class BTreeTests
    {
        private readonly int[] input = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
        
        [Test]
        public void Insert()
        {
            var tree = CreateIntTree(input, 3);
            var root = GetRoot(tree);
            tree.Insert(110, 110);

            Assert.AreEqual(root.Entries.Select(e => e.Key), new int[] { 30, 60 });
            Assert.AreEqual(root.Children.Last().Entries.Select(e => e.Key), new int[] { 70, 80, 90, 100, 110 });
        }
      
        
        [Test]
        public void InsertWithSplitting()
        {
            var tree = CreateIntTree(input, 3);
            var root = GetRoot(tree);
            tree.Insert(110, 110);
            tree.Insert(120, 120);
            
            Assert.AreEqual(root.Entries.Select(e => e.Key), new int[] { 30, 60, 90 });
            Assert.AreEqual(root.Children.Last().Entries.Select(e => e.Key), new int[] { 100, 110, 120 });
        }
        
        [Test]
        public void SearchNotExisitng()
        {
            var tree = CreateIntTree(input, 3);
            var entry1 = tree.Search(10);
            var entry2 = tree.Search(20);
            Assert.AreNotEqual(null, entry1);
            Assert.AreNotEqual(null, entry2);
            Assert.AreEqual(10, entry1.Value);
            Assert.AreEqual(20, entry2.Value);
        }
        
        [Test]
        public void Exceptions()
        {
            Assert.Throws<ArgumentException>(() => new BTree<int, int>(1));
            BTree<string, string> tree = CreateStringTree(new string[] { "10", "20", "30"}, 3);
            Assert.Throws<ArgumentException>(() => tree.Insert(null, "100"));
            Assert.Throws<ArgumentException>(() => tree.Search(null));
            Assert.Throws<ArgumentException>(() => tree.Remove(null));
        }
        
        [Test]
        public void Remove()
        {
            var tree = CreateIntTree(input, 3);
            var root = GetRoot(tree);
            tree.Remove(80);
            
            Assert.AreEqual(root.Entries.Select(e => e.Key), new int[] { 30, 60 });
            Assert.AreEqual(root.Children.Last().Entries.Select(e => e.Key), new int[] { 70, 90, 100 });

            tree.Remove(100);
            
            Assert.AreEqual(root.Entries.Select(e => e.Key), new int[] { 30, 60});
            Assert.AreEqual(root.Children.Last().Entries.Select(e => e.Key), new int[] { 70, 90 });
        }
        
        [Test]
        public void RemoveWithTransfer()
        {
            var tree = CreateIntTree(input, 3);
            var root = GetRoot(tree);
            tree.Remove(50);
            
            Assert.AreEqual(root.Entries.Select(e => e.Key), new int[] { 30, 70 });
            Assert.AreEqual(root.Children.Last().Entries.Select(e => e.Key), new int[] { 80, 90, 100 });
        }
        
        private BTree<string, string> CreateStringTree(string[] keys, int degree)
        {
            var tree = new BTree<string, string>(degree);
            foreach (var key in keys)
                tree.Insert(key, key);
            return tree;
        }
        
        private BTree<int, int> CreateIntTree(int[] keys, int degree)
        {
            var tree = new BTree<int, int>(degree);
            foreach (var key in keys)
                tree.Insert(key, key);
            return tree;
        }
        
        private Node<int, int> GetRoot(BTree<int, int> tree)
        { 
            var prop = typeof(BTree<int, int>).GetProperty("Root", BindingFlags.NonPublic | BindingFlags.Instance);
            return (Node<int, int>)prop.GetValue(tree);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BTree
{
    public class Node<T, V>
    {
        public List<Node<T, V>> Children { get; set; }
        public List<Entry<T, V>> Entries { get; set; }

        private int degree;

        public Node(int degree)
        {
            this.degree = degree;
            Children = new List<Node<T, V>>(degree);
            Entries = new List<Entry<T, V>>(degree);
        }
        
        public bool IsLeaf { get { return Children.Count == 0; } }
        public bool HasReachedMaxEntries { get { return Entries.Count == (2 * degree) - 1; } }
        public bool HasReachedMinEntries { get { return Entries.Count == degree - 1; } }

        public override string ToString() => String.Join( " ", Entries);
    }
}